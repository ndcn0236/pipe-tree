# Renamed to fsl-pipe
This project has been renamed to `fsl-pipe`, which can be found at:
- Repository: https://git.fmrib.ox.ac.uk/ndcn0236/fsl-pipe
- Documentation: https://open.win.ox.ac.uk/pages/ndcn0236/fsl-pipe
- PYPI: https://pypi.org/project/fsl-pipe/
- Conda-forge: https://anaconda.org/conda-forge/fsl-pipe

Please, switch to using `fsl-pipe`, which can be done by following these steps: 
- Install `fsl-pipe` using `conda install fsl-pipe` (requires [conda-forge](https://conda-forge.org/docs/user/introduction.html#how-can-i-install-packages-from-conda-forge) channel) or `pip install fsl-pipe`
- In your code replace all mentions of `pipe_tree` to `fsl_pipe`, for example `from pipe_tree import Pipeline, In, Out` becomes `from fsl_pipe import Pipeline, In, Out`
- That is all! You can now run your pipeline again.

# Old README
[![Documentation](https://img.shields.io/badge/Documentation-pipe--tree-blue)](https://open.win.ox.ac.uk/pages/ndcn0236/pipe-tree)
[![File-tree Documentation](https://img.shields.io/badge/Documentation-file--tree-blue)](https://open.win.ox.ac.uk/pages/ndcn0236/file-tree)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6577070.svg)](https://doi.org/10.5281/zenodo.6577070)
[![Pipeline status](https://git.fmrib.ox.ac.uk/ndcn0236/pipe-tree/badges/main/pipeline.svg)](https://git.fmrib.ox.ac.uk/ndcn0236/pipe-tree/-/pipelines/latest)
[![Coverage report](https://git.fmrib.ox.ac.uk/ndcn0236/pipe-tree/badges/main/coverage.svg)](https://open.win.ox.ac.uk/pages/ndcn0236/pipe-tree/htmlcov)

Declative pipelines based on Filetrees

# Installation
```shell
pip install git+https://git.fmrib.ox.ac.uk/ndcn0236/pipe-tree.git
```

Any bug reports and feature requests are very welcome (see [issue tracker](https://git.fmrib.ox.ac.uk/ndcn0236/pipe-tree/-/issues)).

# Setting up local test environment
First clone the repository:
```shell
git clone https://git.fmrib.ox.ac.uk/ndcn0236/pipe-tree.git
```

Then, we install the package in an interactive manner:
```shell
cd pipe-tree
pip install -e .
```

## Running tests
Tests are run using the [pytest](https://docs.pytest.org) framework. After installation (`pip install pytest`) they can be run from the project root as:
```shell
pytest src/tests
```

## Compiling documentation
The documentation is build using [sphinx](https://www.sphinx-doc.org/en/master/). After installation (`pip install sphinx`) run:
```shell
cd doc
sphinx-build source build
open build/index.html
```

## Contributing
[Merge requests](https://git.fmrib.ox.ac.uk/ndcn0236/pipe-tree/-/merge_requests) with any bug fixes or documentation updates are always welcome. 

For new features, please raise an [issue](https://git.fmrib.ox.ac.uk/ndcn0236/pipe-tree/-/issues) to allow for discussion before you spend the time implementing them.

## Releasing new versions
- Run `bump2version` (install using `pip install bump2version`)
- Push to gitlab



