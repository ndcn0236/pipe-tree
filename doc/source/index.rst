Welcome to pipe-tree's documentation!
=====================================

.. note::
   The `pipe-tree` package has been renamed to `fsl-pipe`. Please switch to this new package following instructions on the README (https://git.fmrib.ox.ac.uk/ndcn0236/pipe-tree).
   The documentation for `fsl-pipe` can be found at https://open.win.ox.ac.uk/pages/ndcn0236/fsl-pipe

Framework to build pipelines in a declerative manner based on FileTrees.
Pipe-tree has the following features:

   - The directory tree containing the input, intermediate, and output files are defined separately from the pipeline code in the convenient `file-tree <https://open.win.ox.ac.uk/pages/ndcn0236/file-tree/>`_ format
   - The pipeline writer only defines the individual recipes in regular python code (e.g., how to convert file A into file B or file B into C), which pipe-tree will stitch together into a complete pipeline.
   - Users can run part of the pipeline by defining which files they actually want to produce.
   - By default, the pipeline will skip any jobs for which the output files already exist.
   - Jobs can be run locally in sequence as normal python functions or in parallel by submitting to a cluster queue (using `fsl_sub <https://git.fmrib.ox.ac.uk/fsl/fsl_sub>`_) or by using `dask <https://dask.org/>`_.

Install using
::

   pip install git+https://git.fmrib.ox.ac.uk/ndcn0236/pipe-tree.git

Typical usage:

.. code-block:: python

    from pipe_tree import pipe, In, Out, Ref, Var

    @pipe
    def job(input_file: In, output_file: Out):
        # code to convert `input_file` to `output_file`

    pipe.cli()  # runs command line interface

.. toctree::
   :maxdepth: 3

   tutorial
   pipe-tree