pipe\_tree package
==================

.. automodule:: pipe_tree
   :members:
   :undoc-members:
   :show-inheritance:

pipe\_tree.pipeline module
--------------------------

.. automodule:: pipe_tree.pipeline
   :members:
   :undoc-members:
   :show-inheritance:

pipe\_tree.job module
---------------------

.. automodule:: pipe_tree.job
   :members:
   :undoc-members:
   :show-inheritance:

pipe\_tree.datalad module
-------------------------

.. automodule:: pipe_tree.datalad
   :members:
   :undoc-members:
   :show-inheritance: