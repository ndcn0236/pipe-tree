"""
Declarative pipeline definition based on filetrees.

Typical usage:

.. code-block:: python

    from pipe_tree import pipe, In, Out, Ref, Var

    @pipe
    def job(input_file: In, output_file: Out):
        # code to convert `input_file` to `output_file`

    pipe.cli()  # runs command line interface
"""
from warnings import warn
warn("The `pipe-tree` package has been renamed to `fsl-pipe`. Please switch to this new package following instructions at https://git.fmrib.ox.ac.uk/fsl/pipe_tree.")
from .pipeline import Pipeline, pipe, In, Out, Ref, Var
from .job import update_closure
__version__ = '0.6.0'