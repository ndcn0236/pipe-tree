from pipe_tree import Pipeline, In, Out, Ref, testing, Var
from pipe_tree.job import OutputMissing, InputMissingPipe
from file_tree import FileTree
import pytest
import os
from fsl.utils.tempdir import tempdir


def test_linear_pipe():
    tree = FileTree.from_string("A.txt\nB.txt\nC.txt\n")
    pipe = Pipeline()

    @pipe
    def gen_A(A: Out):
        testing.touch(A)

    @pipe
    def gen_B(B: Out):
        testing.touch(B)

    @pipe
    def gen_C(A: In, B: In, C: Out):
        testing.touch(C)

    jobs = pipe.generate_jobs(tree)
    assert len(jobs.jobs) == 3

    job_a = jobs.filter(['A'])
    assert len(job_a.jobs) == 1
    assert job_a.jobs[0].kwargs == {'A': 'A.txt'}

    assert len(jobs.filter(['A', 'B']).jobs) == 2
    assert len(jobs.filter(['C']).jobs) == 3


def test_iter_pipe():
    tree = FileTree.from_string("{subject}\n    A.txt\n    B.txt\nC.txt\n")
    tree.placeholders['subject'] = ('01', '02')
    pipe = Pipeline()

    @pipe
    def gen_A(A: Out, subject: Var(no_iter=True)):
        for fn in A.data.flatten():
            testing.touch(fn)

    @pipe
    def gen_B(B: Out, subject: Var):
        testing.touch(B)

    @pipe
    def gen_C(A: In, B: In, C: Out, subject: Var(no_iter=True)):
        testing.touch(C)

    jobs = pipe.generate_jobs(tree)
    assert len(jobs.jobs) == 4

    job_a = jobs.filter(['A'])
    assert job_a.jobs == jobs.filter(['*/A.txt']).jobs
    assert len(job_a.jobs) == 1
    assert list(job_a.jobs[0].kwargs['A'].data) == ['01/A.txt', '02/A.txt']
    assert job_a.jobs[0].kwargs['subject'].key == 'subject'
    assert job_a.jobs[0].kwargs['subject'].index == (0, 1)
    assert job_a.jobs[0].kwargs['subject'].value == ('01', '02')

    # Testing filtering with pattern matching
    assert set(jobs.jobs) == set(jobs.filter(['C.txt']).jobs)
    assert len(jobs.filter(['01/?.txt']).jobs) == 2

    assert len(jobs.filter(['A', 'B']).jobs) == 3
    for idx, s, job in zip(range(2), ('01', '02'), jobs.filter(['B']).jobs):
        assert job.kwargs['B'] == s + '/B.txt'
        assert job.kwargs['subject'].key == 'subject'
        assert job.kwargs['subject'].index == idx
        assert job.kwargs['subject'].value == s
    assert len(jobs.filter(['C']).jobs) == 4
    job_c = jobs.filter(['C']).jobs[-1]
    assert job_c.kwargs['C'] == 'C.txt'
    assert list(job_c.kwargs['A'].data) == ['01/A.txt', '02/A.txt']
    assert list(job_c.kwargs['B'].data) == ['01/B.txt', '02/B.txt']
    s = job_c.kwargs['subject']
    assert s.key == 'subject'
    assert s.index == (0, 1)
    assert s.value == ('01', '02')


def test_submit_check():
    Pipeline(default_submit={'jobtime': '10'})
    with pytest.raises(ValueError):
        Pipeline(default_submit={'unknown_parameter': '10'})


def test_template_glob():
    pipe = Pipeline()
    tree = FileTree.from_string("""
    base_file.txt
    base_other_file.txt
    base_f0.txt
    base_f1.txt
    unrelated.txt
    """)

    @pipe
    def function(all_base: Ref("base_*"), all_idx: Out("base_f?"), empty: Ref("no_files*")):
        assert len(all_base) == 4
        for t in all_base:
            assert all_base[t] == t + '.txt'

        assert len(all_idx) == 2
        for t in all_idx:
            assert all_base[t] == t + '.txt'
            assert all_idx[t] == t + '.txt'

        assert len(empty) == 0

    job = pipe.scripts[0]
    assert job.filter_templates(True, tree.template_keys()) == {'base_f1', 'base_f0'}
    assert job.filter_templates(False, tree.template_keys()) == set()

    with pytest.raises(OutputMissing):
        pipe.generate_jobs(tree).run()

def test_optional_input():
    for _ in range(20):
        for optional in (False, True):
            for skip_missing in (False, True):
                for target in ({"combined"}, None, {"combined"}):
                    with tempdir():
                        pipe = Pipeline()

                        @pipe(kwargs=dict(input=In('dataB'), output=Out('procB')))
                        @pipe(kwargs=dict(input=In('dataA'), output=Out('procA')))
                        def process_input(input, output):
                            assert os.path.exists(input)
                            testing.touch(output)

                        @pipe
                        def combined_processed_data(procA: In, procB: In(optional=optional), combined: Out):
                            with open(combined, 'w') as f:
                                f.write(f"{os.path.exists(procA)} {os.path.exists(procB)}")

                        testing.touch("dataA")
                        tree = FileTree.from_string("""
                        dataA
                        procA
                        dataB
                        procB
                        combined
                        """)

                        if skip_missing or (optional and target is not None):
                            jobs = pipe.generate_jobs(tree).filter(target, skip_missing=True)
                            expected_njobs = 1 if target is None else 0
                            if optional:
                                expected_njobs = 2
                            assert len(jobs) == expected_njobs
                            jobs.run()
                            if optional:
                                assert os.path.exists("combined")
                                assert os.path.exists("procA")
                                assert not os.path.exists("procB")
                                assert open("combined", "r").read() == "True False"
                            else:
                                assert not os.path.exists("combined")
                                if target is None:
                                    assert os.path.exists("procA")
                                else:
                                    assert not os.path.exists("procA")
                            assert not os.path.exists("procB")
                        else:
                            with pytest.raises(InputMissingPipe):
                                pipe.generate_jobs(tree).filter(target, skip_missing=False)
                        
def test_batching():
    pipe = Pipeline()

    @pipe(batch="base", submit={"jobtime": 1, "jobram": 100})
    def gen_A(A: Out, subject: Var):
        testing.touch(A)

    @pipe(batch="base", submit={"jobtime": 3, "jobram": 10})
    def gen_B(A: In, B: Out):
        testing.touch(B)

    @pipe(submit={"coprocessor": "cuda"})
    def gen_C(B: In, C: Out):
        testing.touch(C)

    @pipe(batch="base", submit={"jobtime": 9})
    def gen_D(B: In, C: In, D:Out):
        testing.touch(D)

    @pipe(batch="base", submit={"jobtime": 81})
    def gen_E(A: In, E: Out):
        testing.touch(E)

    @pipe(batch="base")
    def gen_F(D: In, F:Out):
        testing.touch(F)

    @pipe(batch="base", no_iter={"subject"}, submit={"jobtime": 300})
    def combine(F: In, G: Out):
        testing.touch(G)

    tree = FileTree.from_string("""
    subject=01,02

    sub-{subject}
        A.txt
        B.txt
        C.txt
        D.txt
        E.txt
        F.txt
    G.txt
    """)


    for use_placeholders in (False, True, ["subject"], ["session"]):
        placeholders_used = use_placeholders in (True, ["subject"])
        for use_label in (False, True):
            for only_connected in (True, False):
                jobs = pipe.generate_jobs(tree)
                assert len(jobs) == 13
            
                batches = jobs.batch(use_label=use_label, only_connected=only_connected, use_placeholders=use_placeholders)

                (n_expected, expected_times, expected_names) = {
                    (False, False, False): (3, {None, 170, 318}, {"base_gen_a-gen_b-gen_e", "gen_c", "base_combine-gen_d-gen_f"}),
                    (False, True, False): (4, {None, 170, 318}, {"base_gen_a-gen_b-gen_e", "gen_c_subject-01", "gen_c_subject-02", "base_combine-gen_d-gen_f"}),
                    (False, False, True): (5, {None, 85, 318}, {"base_gen_a-gen_b-gen_e_subject-01", "gen_c_subject-01", "base_gen_a-gen_b-gen_e_subject-02", "gen_c_subject-02", "base_combine-gen_d-gen_f"}),
                    (False, True, True): (5, {None, 85, 318}, {"base_gen_a-gen_b-gen_e_subject-01", "gen_c_subject-01", "base_gen_a-gen_b-gen_e_subject-02", "gen_c_subject-02", "base_combine-gen_d-gen_f"}),
                    (True, False, False): (7, {None, 9, 85, 300}, {"base_gen_a-gen_b-gen_e_subject-01", "gen_c_subject-01", "base_gen_d-gen_f_subject-01", "base_gen_a-gen_b-gen_e_subject-02", "gen_c_subject-02", "base_gen_d-gen_f_subject-02", "combine"}),
                    (True, True, False): (7, {None, 9, 85, 300}, {"base_gen_a-gen_b-gen_e_subject-01", "gen_c_subject-01", "base_gen_d-gen_f_subject-01", "base_gen_a-gen_b-gen_e_subject-02", "gen_c_subject-02", "base_gen_d-gen_f_subject-02", "combine"}),
                    (True, False, True): (7, {None, 9, 85, 300}, {"base_gen_a-gen_b-gen_e_subject-01", "gen_c_subject-01", "base_gen_d-gen_f_subject-01", "base_gen_a-gen_b-gen_e_subject-02", "gen_c_subject-02", "base_gen_d-gen_f_subject-02", "combine"}),
                    (True, True, True): (7, {None, 9, 85, 300}, {"base_gen_a-gen_b-gen_e_subject-01", "gen_c_subject-01", "base_gen_d-gen_f_subject-01", "base_gen_a-gen_b-gen_e_subject-02", "gen_c_subject-02", "base_gen_d-gen_f_subject-02", "combine"}),
                }[(placeholders_used, use_label, only_connected)]

                assert len(batches) == n_expected

                jobtimes = set(batch.submit_params.get("jobtime", None) for batch in batches.jobs)
                assert jobtimes == expected_times

                jobrams = set(batch.submit_params.get("jobram", None) for batch in batches.jobs)
                assert jobrams == {None, 100}

                jobnames = set(batch.job_name() for batch in batches.jobs)
                assert jobnames == expected_names

                with tempdir():
                    os.mkdir("sub-01")
                    testing.touch("sub-01/A.txt")

                    jobs = pipe.generate_jobs(tree).filter(None)
                    assert len(jobs) == 12

                    batches = jobs.batch(use_label, only_connected=only_connected, use_placeholders=use_placeholders)

                    (n_expected, expected_times, expected_names) = {
                        (False, False, False): (3, {None, 169, 318}, {"base_gen_a-gen_b-gen_e", "gen_c", "base_combine-gen_d-gen_f"}),
                        (False, True, False): (4, {None, 169, 318}, {"base_gen_a-gen_b-gen_e", "gen_c_subject-01", "gen_c_subject-02", "base_combine-gen_d-gen_f"}),
                        (False, False, True): (6, {None, 3, 81, 85, 318}, {"gen_b_subject-01", "gen_e_subject-01", "gen_c_subject-01", "base_gen_a-gen_b-gen_e_subject-02", "gen_c_subject-02", "base_combine-gen_d-gen_f"}),
                        (False, True, True): (6, {None, 3, 81, 85, 318}, {"gen_b_subject-01", "gen_e_subject-01", "gen_c_subject-01", "base_gen_a-gen_b-gen_e_subject-02", "gen_c_subject-02", "base_combine-gen_d-gen_f"}),
                        (True, False, False): (7, {None, 9, 84, 85, 300}, {"base_gen_b-gen_e_subject-01", "gen_c_subject-01", "base_gen_d-gen_f_subject-01", "base_gen_a-gen_b-gen_e_subject-02", "gen_c_subject-02", "base_gen_d-gen_f_subject-02", "combine"}),
                        (True, True, False): (7, {None, 9, 84, 85, 300}, {"base_gen_b-gen_e_subject-01", "gen_c_subject-01", "base_gen_d-gen_f_subject-01", "base_gen_a-gen_b-gen_e_subject-02", "gen_c_subject-02", "base_gen_d-gen_f_subject-02", "combine"}),
                        (True, False, True): (8, {None, 3, 9, 81, 85, 300}, {"gen_b_subject-01", "gen_e_subject-01", "gen_c_subject-01", "base_gen_d-gen_f_subject-01", "base_gen_a-gen_b-gen_e_subject-02", "gen_c_subject-02", "base_gen_d-gen_f_subject-02", "combine"}),
                        (True, True, True): (8, {None, 3, 9, 81, 85, 300}, {"gen_b_subject-01", "gen_e_subject-01", "gen_c_subject-01", "base_gen_d-gen_f_subject-01", "base_gen_a-gen_b-gen_e_subject-02", "gen_c_subject-02", "base_gen_d-gen_f_subject-02", "combine"}),
                    }[(placeholders_used, use_label, only_connected)]

                    assert len(batches) == n_expected

                    jobtimes = set(batch.submit_params.get("jobtime", None) for batch in batches.jobs)
                    assert jobtimes == expected_times

                    jobnames = set(batch.job_name() for batch in batches.jobs)
                    assert jobnames == expected_names

                    jobrams = set(batch.submit_params.get("jobram", None) for batch in batches.jobs)
                    if only_connected or placeholders_used:
                        assert jobrams == {None, 10, 100}
                    else:
                        assert jobrams == {None, 100}

                    batches.run()
                    assert os.path.exists("G.txt")
